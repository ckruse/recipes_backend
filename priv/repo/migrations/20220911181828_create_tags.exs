defmodule Recipes.Repo.Migrations.CreateTags do
  use Ecto.Migration

  def change do
    create table(:tags) do
      add :name, :string

      timestamps()
    end

    create table(:recipes_tags, primary_key: false) do
      add :recipe_id, references(:recipes, on_delete: :delete_all, on_update: :update_all),
        primary_key: true,
        null: false

      add :tag_id, references(:tags, on_delete: :delete_all, on_update: :update_all),
        primary_key: true,
        null: false
    end
  end
end
