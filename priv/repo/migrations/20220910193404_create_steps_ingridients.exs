defmodule Recipes.Repo.Migrations.CreateStepsIngridients do
  use Ecto.Migration

  def change do
    create table(:steps_ingridients) do
      add :step_id, references(:steps, on_delete: :delete_all, on_update: :update_all), null: false
      add :ingredient_id, references(:ingredients, on_delete: :restrict, on_update: :update_all), null: false

      add :amount, :float, null: false
      add :unit, :string, null: false
      add :annotation, :string

      timestamps()
    end

    create index(:steps_ingridients, [:step_id])
    create index(:steps_ingridients, [:ingredient_id])
  end
end
