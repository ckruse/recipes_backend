defmodule Recipes.Repo.Migrations.AddCookingTimesToSteps do
  use Ecto.Migration

  def change do
    alter table(:steps) do
      add :preparation_time, :integer, null: false, default: 0
      add :cooking_time, :integer, null: false, default: 0
    end
  end
end
