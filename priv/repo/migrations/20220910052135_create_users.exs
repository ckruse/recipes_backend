defmodule Recipes.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :string, null: false
      add :active, :boolean, default: false, null: false
      add :encrypted_password, :string

      add :avatar, :string

      add :name, :string
      add :role, :string

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
