defmodule Recipes.Repo.Migrations.CreateRecipes do
  use Ecto.Migration

  def change do
    create table(:recipes) do
      add :name, :string, null: false
      add :description, :text
      add :owner_id, references(:users, on_delete: :nilify_all, on_update: :update_all)

      timestamps()
    end

    create index(:recipes, [:owner_id])
  end
end
