defmodule Recipes.Repo.Migrations.CreateSteps do
  use Ecto.Migration

  def change do
    create table(:steps) do
      add :recipe_id, references(:recipes, on_delete: :delete_all, on_update: :update_all), null: false
      add :position, :integer, null: false
      add :description, :text

      timestamps()
    end

    create index(:steps, [:recipe_id])
  end
end
