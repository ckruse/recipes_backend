defmodule Recipes.Repo.Migrations.AddIngredientUnits do
  use Ecto.Migration

  def change do
    create table(:ingredient_units) do
      add :ingredient_id, references(:ingredients, on_delete: :delete_all, on_update: :update_all), null: false
      add :identifier, :string, null: false
      add :base_value, :float, null: false

      timestamps()
    end

    alter table(:steps_ingridients) do
      add :unit_id, references(:ingredient_units, on_delete: :delete_all, on_update: :update_all)
      remove :unit
    end

    create index(:ingredient_units, [:ingredient_id, :identifier], unique: true)
    create index(:steps_ingridients, [:unit_id])
  end
end
