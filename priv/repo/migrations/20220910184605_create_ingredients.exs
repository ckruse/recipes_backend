defmodule Recipes.Repo.Migrations.CreateIngredients do
  use Ecto.Migration

  def change do
    create table(:ingredients) do
      add :name, :string, null: false
      add :reference, :string, null: false
      add :carbs, :float, null: false, default: 0
      add :fat, :float, null: false, default: 0
      add :proteins, :float, null: false, default: 0
      add :alc, :float, null: false, default: 0

      timestamps()
    end
  end
end
