import Config

config :recipes, RecipesWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: System.fetch_env!("RECIPES_PORT")],
  url: [scheme: "https", host: "recipes.wwwtech.de", port: 443],
  secret_key_base: System.fetch_env!("RECIPES_SECRET_KEY")

config :recipes, Recipes.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.fetch_env!("RECIPES_DB_USERNAME"),
  password: System.fetch_env!("RECIPES_DB_PASSWORD"),
  database: System.fetch_env!("RECIPES_DB_NAME"),
  hostname: System.fetch_env!("RECIPES_DB_HOST"),
  pool_size: 4,
  ssl: System.fetch_env("TT_DB_SSL") != {:ok, "false"}

config :recipes, Recipes.Guardian,
  issuer: "Recipes",
  secret_key: System.fetch_env!("RECIPES_GUARDIAN_KEY")

config :recipes,
  avatar_dir: System.fetch_env!("RECIPES_AVATAR_DIR"),
  uploads_dir: System.fetch_env!("RECIPES_UPLOADS_DIR"),
  avatar_url: "/uploads/avatars",
  recipe_image_url: "/uploads/recipes",
  recipe_image_dir: System.fetch_env!("RECIPES_IMAGE_DIR")
