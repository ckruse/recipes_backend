import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :recipes, Recipes.Repo,
  hostname: "localhost",
  database: "recipes_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :recipes, RecipesWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "PCcIPUYs+4qkLBtK2i5d1t9OMbzZ8bvNEctEOc46QqqvBa7oUD1bj4ytDSlwserL",
  server: false

# In test we don't send emails.
config :recipes, Recipes.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

config :recipes, Recipes.Guardian,
  issuer: "Recipes",
  # don't bother... this is not a valid secret
  secret_key: "r7ZDLPlM3e8Zn6tmeTq+8BKejLWysJYTcyUzO3zoP3ukKm5IpOfHKGHM7L5rXEJE"
