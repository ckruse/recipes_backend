defmodule Recipes.IngredientsTest do
  use Recipes.DataCase

  alias Recipes.Ingredients

  describe "ingredients" do
    alias Recipes.Ingredients.Ingredient

    @invalid_attrs %{reference: nil, alc: nil, carbs: nil, fat: nil, name: nil, proteins: nil}

    test "list_ingredients/0 returns all ingredients" do
      ingredient = insert(:ingredient)
      assert Ingredients.list_ingredients() == [ingredient]
    end

    test "get_ingredient!/1 returns the ingredient with given id" do
      ingredient = insert(:ingredient)
      assert Ingredients.get_ingredient!(ingredient.id) == ingredient
    end

    test "get_ingredient/1 returns the ingredient with given id" do
      ingredient = insert(:ingredient)
      assert Ingredients.get_ingredient(ingredient.id) == ingredient
    end

    test "create_ingredient/1 with valid data creates a ingredient" do
      valid_attrs = %{alc: 120.5, carbs: 120.5, fat: 120.5, name: "some name", proteins: 120.5}

      assert {:ok, %Ingredient{} = ingredient} = Ingredients.create_ingredient(valid_attrs)
      assert ingredient.alc == 120.5
      assert ingredient.carbs == 120.5
      assert ingredient.fat == 120.5
      assert ingredient.name == "some name"
      assert ingredient.proteins == 120.5
    end

    test "create_ingredient/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Ingredients.create_ingredient(@invalid_attrs)
    end

    test "update_ingredient/2 with valid data updates the ingredient" do
      ingredient = insert(:ingredient)
      update_attrs = %{alc: 456.7, carbs: 456.7, fat: 456.7, name: "some updated name", proteins: 456.7}

      assert {:ok, %Ingredient{} = ingredient} = Ingredients.update_ingredient(ingredient, update_attrs)
      assert ingredient.alc == 456.7
      assert ingredient.carbs == 456.7
      assert ingredient.fat == 456.7
      assert ingredient.name == "some updated name"
      assert ingredient.proteins == 456.7
    end

    test "update_ingredient/2 with invalid data returns error changeset" do
      ingredient = insert(:ingredient)
      assert {:error, %Ecto.Changeset{}} = Ingredients.update_ingredient(ingredient, @invalid_attrs)
      assert ingredient == Ingredients.get_ingredient!(ingredient.id)
    end

    test "delete_ingredient/1 deletes the ingredient" do
      ingredient = insert(:ingredient)
      assert {:ok, %Ingredient{}} = Ingredients.delete_ingredient(ingredient)
      assert_raise Ecto.NoResultsError, fn -> Ingredients.get_ingredient!(ingredient.id) end
    end
  end
end
