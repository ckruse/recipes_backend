defmodule Recipes.TagsTest do
  use Recipes.DataCase

  alias Recipes.Tags

  describe "tags" do
    alias Recipes.Tags.Tag

    @invalid_attrs %{name: nil}

    test "list_tags/0 returns all tags" do
      tag = insert(:tag)
      assert equal_objects(Tags.list_tags(), [tag])
    end

    test "get_tag!/1 returns the tag with given id" do
      tag = insert(:tag)
      assert equal_objects(Tags.get_tag!(tag.id), tag)
    end

    test "get_tag/1 returns the tag with given id" do
      tag = insert(:tag)
      assert equal_objects(Tags.get_tag(tag.id), tag)
    end

    test "create_tag/1 with valid data creates a tag" do
      valid_attrs = %{name: "some tag"}

      assert {:ok, %Tag{} = tag} = Tags.create_tag(valid_attrs)
      assert tag.name == "some tag"
    end

    test "create_tag/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Tags.create_tag(@invalid_attrs)
    end

    test "update_tag/2 with valid data updates the tag" do
      tag = insert(:tag)
      update_attrs = %{name: "some updated tag"}

      assert {:ok, %Tag{} = tag} = Tags.update_tag(tag, update_attrs)
      assert tag.name == "some updated tag"
    end

    test "update_tag/2 with invalid data returns error changeset" do
      tag = insert(:tag)
      assert {:error, %Ecto.Changeset{}} = Tags.update_tag(tag, @invalid_attrs)
      assert equal_objects(tag, Tags.get_tag!(tag.id))
    end

    test "delete_tag/1 deletes the tag" do
      tag = insert(:tag)
      assert {:ok, %Tag{}} = Tags.delete_tag(tag)
      assert_raise Ecto.NoResultsError, fn -> Tags.get_tag!(tag.id) end
    end
  end
end
