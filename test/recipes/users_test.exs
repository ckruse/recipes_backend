defmodule Recipes.UsersTest do
  use Recipes.DataCase

  alias Recipes.Users

  describe "users" do
    alias Recipes.Users.User

    @invalid_attrs %{active: nil, email: nil, name: nil}

    test "list_users/0 returns all users" do
      user = insert(:user)
      assert equal_objects(Users.list_users(), [user])
    end

    test "get_user!/1 returns the user with given id" do
      user = insert(:user)
      assert equal_objects(Users.get_user!(user.id), user)
    end

    test "get_user_by/1 returns the user with given clauses" do
      user = insert(:user)
      assert equal_objects(Users.get_user_by(email: user.email), user)
    end

    test "get_user/1 returns the user with given id" do
      user = insert(:user)
      assert equal_objects(Users.get_user(user.id), user)
    end

    test "create_user/1 with valid data creates a user" do
      valid_attrs = %{
        active: true,
        email: "some email",
        name: "some name"
      }

      assert {:ok, %User{} = user} = Users.create_user(valid_attrs, :root)
      assert user.active == true
      assert user.email == "some email"
      assert user.name == "some name"
      assert user.role == :root
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Users.create_user(@invalid_attrs, :root)
    end

    test "update_user/2 with valid data updates the user" do
      user = insert(:user)

      update_attrs = %{
        active: false,
        email: "some updated email",
        name: "some updated name"
      }

      assert {:ok, %User{} = user} = Users.update_user(user, update_attrs, :root)
      assert user.active == false
      assert user.email == "some updated email"
      assert user.name == "some updated name"
      assert user.role == :root
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = insert(:user)
      assert {:error, %Ecto.Changeset{}} = Users.update_user(user, @invalid_attrs)
      assert equal_objects(user, Users.get_user!(user.id))
    end

    test "delete_user/1 deletes the user" do
      user = insert(:user)
      assert {:ok, %User{}} = Users.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Users.get_user!(user.id) end
    end

    test "token_sign_in/2 returns a token" do
      {:ok, user} = Users.create_user(params_for(:user, password: "123456789012"))
      assert {:ok, %User{}, _token, _claims} = Users.token_sign_in(user, "123456789012")
    end
  end
end
