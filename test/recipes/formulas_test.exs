defmodule Recipes.FormulasTest do
  use Recipes.DataCase

  alias Recipes.Formulas

  describe "recipes" do
    alias Recipes.Formulas.Recipe

    @invalid_attrs %{description: nil, name: nil}

    test "list_recipes/0 returns all recipes" do
      recipe = insert(:recipe)
      assert equal_objects(Formulas.list_recipes(), [recipe])
    end

    test "count_recipes/0 returns all recipes" do
      insert(:recipe)
      assert Formulas.count_recipes() == 1
    end

    test "get_recipe!/1 returns the recipe with given id" do
      recipe = insert(:recipe)
      assert equal_objects(Formulas.get_recipe!(recipe.id), recipe)
    end

    test "get_recipe/1 returns the recipe with given id" do
      recipe = insert(:recipe)
      assert equal_objects(Formulas.get_recipe(recipe.id), recipe)
    end

    test "create_recipe/1 with valid data creates a recipe" do
      user = insert(:user)
      valid_attrs = %{description: "some description", name: "some name"}

      assert {:ok, %Recipe{} = recipe} = Formulas.create_recipe(user, valid_attrs)
      assert recipe.description == "some description"
      assert recipe.name == "some name"
    end

    test "create_recipe/1 with invalid data returns error changeset" do
      user = insert(:user)
      assert {:error, %Ecto.Changeset{}} = Formulas.create_recipe(user, @invalid_attrs)
    end

    test "update_recipe/2 with valid data updates the recipe" do
      recipe = insert(:recipe)
      update_attrs = %{description: "some updated description", name: "some updated name"}

      assert {:ok, %Recipe{} = recipe} = Formulas.update_recipe(recipe, update_attrs)
      assert recipe.description == "some updated description"
      assert recipe.name == "some updated name"
    end

    test "update_recipe/2 with invalid data returns error changeset" do
      recipe = insert(:recipe)
      assert {:error, %Ecto.Changeset{}} = Formulas.update_recipe(recipe, @invalid_attrs)
      assert equal_objects(recipe, Formulas.get_recipe!(recipe.id))
    end

    test "delete_recipe/1 deletes the recipe" do
      recipe = insert(:recipe)
      assert {:ok, %Recipe{}} = Formulas.delete_recipe(recipe)
      assert_raise Ecto.NoResultsError, fn -> Formulas.get_recipe!(recipe.id) end
    end
  end

  describe "steps" do
    alias Recipes.Formulas.Step

    @invalid_attrs %{annotation: nil, description: nil, position: nil}

    test "list_steps/0 returns all steps" do
      step = insert(:step)
      assert equal_objects(Formulas.list_steps(step.recipe), [step])
    end

    test "get_step!/1 returns the step with given id" do
      step = insert(:step)
      assert equal_objects(Formulas.get_step!(step.id), step)
    end

    test "get_step/1 returns the step with given id" do
      step = insert(:step)
      assert equal_objects(Formulas.get_step(step.id), step)
    end

    test "create_step/1 with valid data creates a step" do
      recipe = insert(:recipe)
      valid_attrs = params_for(:step, recipe: recipe)

      assert {:ok, %Step{} = step} = Formulas.create_step(recipe, valid_attrs)
      assert step.description == valid_attrs[:description]
      assert step.position == valid_attrs[:position]
      assert step.recipe_id == recipe.id
    end

    test "create_step/1 with invalid data returns error changeset" do
      recipe = insert(:recipe)
      assert {:error, %Ecto.Changeset{}} = Formulas.create_step(recipe, @invalid_attrs)
    end

    test "update_step/2 with valid data updates the step" do
      step = insert(:step)
      update_attrs = %{annotation: "some updated annotation", description: "some updated description", position: 43}

      assert {:ok, %Step{} = step} = Formulas.update_step(step, update_attrs)
      assert step.description == "some updated description"
      assert step.position == 43
    end

    test "update_step/2 with invalid data returns error changeset" do
      step = insert(:step)
      assert {:error, %Ecto.Changeset{}} = Formulas.update_step(step, @invalid_attrs)
      assert equal_objects(step, Formulas.get_step!(step.id))
    end

    test "delete_step/1 deletes the step" do
      step = insert(:step)
      assert {:ok, %Step{}} = Formulas.delete_step(step)
      assert_raise Ecto.NoResultsError, fn -> Formulas.get_step!(step.id) end
    end
  end

  describe "steps_ingridients" do
    alias Recipes.Formulas.StepIngredient

    @invalid_attrs %{amount: nil, annotation: nil, unit: nil}

    test "list_steps_ingridients/0 returns all steps_ingridients" do
      step_ingredient = insert(:step_ingredient)
      assert equal_objects(Formulas.list_step_ingridients(step_ingredient.step), [step_ingredient])
    end

    test "get_step_ingredient!/1 returns the step_ingredient with given id" do
      step_ingredient = insert(:step_ingredient)
      assert equal_objects(Formulas.get_step_ingredient!(step_ingredient.id), step_ingredient)
    end

    test "get_step_ingredient/1 returns the step_ingredient with given id" do
      step_ingredient = insert(:step_ingredient)
      assert equal_objects(Formulas.get_step_ingredient(step_ingredient.id), step_ingredient)
    end

    test "create_step_ingredient/1 with valid data creates a step_ingredient" do
      step = insert(:step)
      ingredient = insert(:ingredient)
      valid_attrs = params_for(:step_ingredient, step: step, ingredient: ingredient)

      assert {:ok, %StepIngredient{} = step_ingredient} = Formulas.create_step_ingredient(valid_attrs)
      assert step_ingredient.amount == valid_attrs[:amount]
      assert step_ingredient.annotation == valid_attrs[:annotation]
      assert step_ingredient.unit == valid_attrs[:unit]
    end

    test "create_step_ingredient/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Formulas.create_step_ingredient(@invalid_attrs)
    end

    test "update_step_ingredient/2 with valid data updates the step_ingredient" do
      step_ingredient = insert(:step_ingredient)
      update_attrs = %{amount: 456.7, annotation: "some updated annotation", unit: "some updated unit"}

      assert {:ok, %StepIngredient{} = step_ingredient} = Formulas.update_step_ingredient(step_ingredient, update_attrs)

      assert step_ingredient.amount == 456.7
      assert step_ingredient.annotation == "some updated annotation"
      assert step_ingredient.unit == "some updated unit"
    end

    test "update_step_ingredient/2 with invalid data returns error changeset" do
      step_ingredient = insert(:step_ingredient)
      assert {:error, %Ecto.Changeset{}} = Formulas.update_step_ingredient(step_ingredient, @invalid_attrs)
      assert equal_objects(step_ingredient, Formulas.get_step_ingredient!(step_ingredient.id))
    end

    test "delete_step_ingredient/1 deletes the step_ingredient" do
      step_ingredient = insert(:step_ingredient)
      assert {:ok, %StepIngredient{}} = Formulas.delete_step_ingredient(step_ingredient)
      assert_raise Ecto.NoResultsError, fn -> Formulas.get_step_ingredient!(step_ingredient.id) end
    end
  end
end
