defmodule RecipesWeb.Schema.RecipeQueriesTest do
  use RecipesWeb.ConnCase

  setup do
    recipes =
      for _ <- 1..10,
          do: insert(:recipe)

    {:ok, %{recipes: recipes}}
  end

  describe "recipes" do
    @query """
    query($search: String, $tags: [String], $limit: Int!, $offset: Int!) {
      recipes(search: $search, tags: $tags, limit: $limit, offset: $offset) {
        id
        name
      }
    }
    """

    test "recipes query returns all recipes", %{conn: conn, recipes: recipes} do
      conn = post conn, "/graphql", query: @query, variables: %{"limit" => 10, "offset" => 0}

      assert json_response(conn, 200) == %{
               "data" => %{"recipes" => Enum.map(recipes, &%{"id" => "#{&1.id}", "name" => &1.name})}
             }
    end

    test "recipes query searches for a recipe", %{conn: conn} do
      recipe = insert(:recipe, name: "Gagh")
      conn = post conn, "/graphql", query: @query, variables: %{"limit" => 10, "offset" => 0, "search" => "gagh"}

      assert json_response(conn, 200) == %{"data" => %{"recipes" => [%{"id" => "#{recipe.id}", "name" => recipe.name}]}}
    end

    test "recipes query searches for tags", %{conn: conn} do
      tag1 = insert(:tag, name: "klingon")
      tag2 = insert(:tag, name: "living")
      recipe = insert(:recipe, tags: [tag1, tag2])

      conn = post conn, "/graphql", query: @query, variables: %{"limit" => 10, "offset" => 0, "tags" => ["klingon"]}
      assert json_response(conn, 200) == %{"data" => %{"recipes" => [%{"id" => "#{recipe.id}", "name" => recipe.name}]}}

      conn = post conn, "/graphql", query: @query, variables: %{"limit" => 10, "offset" => 0, "tags" => ["living"]}
      assert json_response(conn, 200) == %{"data" => %{"recipes" => [%{"id" => "#{recipe.id}", "name" => recipe.name}]}}

      conn =
        post conn, "/graphql",
          query: @query,
          variables: %{"limit" => 10, "offset" => 0, "tags" => ["klingon", "living"]}

      assert json_response(conn, 200) == %{"data" => %{"recipes" => [%{"id" => "#{recipe.id}", "name" => recipe.name}]}}
    end

    test "recipes query applies limit and offset", %{conn: conn, recipes: recipes} do
      conn = post conn, "/graphql", query: @query, variables: %{"limit" => 1, "offset" => 1}
      recipe = Enum.at(recipes, 1)

      assert json_response(conn, 200) == %{"data" => %{"recipes" => [%{"id" => "#{recipe.id}", "name" => recipe.name}]}}
    end
  end

  describe "recipe" do
    @query """
    query($id: ID!) {
      recipe(id: $id) {
        id
        name
      }
    }
    """

    test "recipe field returns the recipe", %{conn: conn, recipes: [recipe | _]} do
      conn = post conn, "/graphql", query: @query, variables: %{"id" => recipe.id}

      assert json_response(conn, 200) == %{"data" => %{"recipe" => %{"id" => "#{recipe.id}", "name" => recipe.name}}}
    end
  end
end
