defmodule Recipes.Factory do
  use ExMachina.Ecto, repo: Recipes.Repo

  def user_factory do
    %Recipes.Users.User{
      email: sequence(:emails, &"email-#{&1}@example.com"),
      active: true,
      role: :root
    }
  end

  def recipe_factory do
    %Recipes.Formulas.Recipe{
      name: sequence(:names, &"Recipe #{&1}"),
      description: "This is a recipe",
      owner: build(:user)
    }
  end

  def step_factory do
    %Recipes.Formulas.Step{
      position: 0,
      description: "This is a step",
      recipe: build(:recipe)
    }
  end

  def step_ingredient_factory do
    %Recipes.Formulas.StepIngredient{
      amount: 1.0,
      unit: "cup",
      annotation: "some annotation",
      ingredient: build(:ingredient),
      step: build(:step)
    }
  end

  def ingredient_factory do
    %Recipes.Ingredients.Ingredient{
      name: sequence(:names, &"Ingredient #{&1}"),
      reference: :g,
      alc: 0.0,
      carbs: 0.0,
      fat: 0.0
    }
  end

  def tag_factory do
    %Recipes.Tags.Tag{
      name: sequence(:names, &"Tag #{&1}")
    }
  end
end
