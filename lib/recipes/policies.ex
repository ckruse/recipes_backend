defmodule Recipes.Policies do
  @callback authorized?(atom, Recipes.Users.User.t() | nil, map) :: boolean()

  @spec authorize(atom, atom, Recipes.Users.User.t() | nil, map) :: :ok | {:error, :unauthorized} | {:error, String.t()}
  def authorize(policy, action, user_or_endpoint, params) do
    if policy.authorized?(action, user_or_endpoint, params),
      do: :ok,
      else: {:error, :unauthorized}
  end
end
