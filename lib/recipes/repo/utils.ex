defmodule Recipes.Repo.Utils do
  import Ecto.Query

  @spec apply_limit(Ecto.Queryable.t(), any, any) :: Ecto.Queryable.t()
  def apply_limit(query, limit_val, offset_val) when is_number(limit_val) and is_number(offset_val) do
    query
    |> limit(^limit_val)
    |> offset(^offset_val)
  end

  def apply_limit(query, _, _), do: query
end
