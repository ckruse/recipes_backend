defmodule Recipes.Tags.Tag do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tags" do
    field :name, :string

    many_to_many :recipes, Recipes.Formulas.Recipe, join_through: "recipes_tags"

    timestamps()
  end

  @doc false
  def changeset(tag, attrs) do
    tag
    |> cast(attrs, [:name])
    |> cast_assoc(:recipes)
    |> validate_required([:name])
  end
end
