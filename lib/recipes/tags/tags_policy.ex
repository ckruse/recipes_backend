defmodule Recipes.Tags.TagsPolicy do
  @behaviour Recipes.Policies

  @impl Recipes.Policies
  def authorized?(_, _, _), do: true
end
