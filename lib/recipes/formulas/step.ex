defmodule Recipes.Formulas.Step do
  use Ecto.Schema
  import Ecto.Changeset

  alias Recipes.Formulas.Recipe

  @type t :: %__MODULE__{
          position: non_neg_integer(),
          preparation_time: non_neg_integer(),
          cooking_time: non_neg_integer(),
          description: String.t() | nil,
          recipe: Recipe.t(),
          step_ingredients: list(Recipes.Formulas.StepIngredient.t()),
          inserted_at: NaiveDateTime.t(),
          updated_at: NaiveDateTime.t()
        }

  schema "steps" do
    field :position, :integer
    field :preparation_time, :integer, default: 0
    field :cooking_time, :integer, default: 0
    field :description, :string

    belongs_to :recipe, Recipe
    has_many :step_ingredients, Recipes.Formulas.StepIngredient

    timestamps()
  end

  @doc false
  def changeset(step, attrs, recipe \\ nil) do
    step
    |> cast(attrs, [:position, :preparation_time, :cooking_time, :description, :recipe_id])
    |> cast_assoc(:step_ingredients, with: &Recipes.Formulas.StepIngredient.changeset/2)
    |> maybe_put_recipe(recipe)
    |> validate_required([:position, :description, :recipe_id])
  end

  defp maybe_put_recipe(%Ecto.Changeset{} = changeset, %Recipe{} = recipe),
    do: put_change(changeset, :recipe_id, recipe.id)

  defp maybe_put_recipe(changeset, _), do: changeset
end
