defmodule Recipes.Formulas.Recipe do
  use Ecto.Schema
  use Waffle.Ecto.Schema

  import Ecto.Changeset

  import Recipes.Utils

  alias Recipes.Tags
  alias Recipes.Users.User

  @type t :: %__MODULE__{
          name: String.t(),
          tags: list(Recipes.Tags.Tag.t()),
          description: String.t() | nil,
          owner: User.t() | nil,
          inserted_at: NaiveDateTime.t(),
          updated_at: NaiveDateTime.t()
        }

  schema "recipes" do
    field :name, :string
    field :description, :string

    field :image, Recipes.Uploaders.RecipeImage.Type

    has_many :steps, Recipes.Formulas.Step
    many_to_many :tags, Tags.Tag, join_through: "recipes_tags"
    belongs_to :owner, User

    timestamps()
  end

  @doc false
  def changeset(recipe, attrs, user \\ nil) do
    recipe
    |> cast(attrs, [:name, :description])
    |> cast_attachments(attrs, [:image])
    |> put_assoc(:tags, get_tags(get_value(attrs, :tags)))
    |> maybe_put_owner(user)
    |> validate_required([:name])
  end

  defp maybe_put_owner(%Ecto.Changeset{} = changeset, %User{} = user),
    do: put_assoc(changeset, :owner, user)

  defp maybe_put_owner(changeset, _), do: changeset

  defp get_tags(ids) when is_blank(ids), do: []
  defp get_tags(ids), do: Tags.get_tags(ids)
end
