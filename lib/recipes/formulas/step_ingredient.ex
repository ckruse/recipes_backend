defmodule Recipes.Formulas.StepIngredient do
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{
          amount: float(),
          annotation: String.t() | nil,
          unit_id: any(),
          step_id: any(),
          ingredient_id: any(),
          unit: Recipes.Ingredients.Unit.t() | nil,
          ingredient: Recipes.Ingredients.Ingredient.t(),
          step: Recipes.Formulas.Step.t(),
          inserted_at: NaiveDateTime.t(),
          updated_at: NaiveDateTime.t()
        }

  schema "steps_ingridients" do
    field :amount, :float
    field :annotation, :string

    belongs_to :step, Recipes.Formulas.Step
    belongs_to :ingredient, Recipes.Ingredients.Ingredient
    belongs_to :unit, Recipes.Ingredients.Unit

    timestamps()
  end

  @doc false
  def changeset(step_ingredients, attrs) do
    step_ingredients
    |> cast(attrs, [:amount, :unit_id, :annotation, :ingredient_id])
    |> validate_required([:amount, :ingredient_id])
  end
end
