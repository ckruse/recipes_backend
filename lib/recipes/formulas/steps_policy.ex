defmodule Recipes.Formulas.StepsPolicy do
  @behaviour Recipes.Policies

  alias Recipes.Users.User

  @impl Recipes.Policies
  def authorized?(:list, _, _), do: true
  def authorized?(:get, _, _), do: true

  def authorized?(_, %User{role: :root}, _), do: true

  def authorized?(:create, %User{}, _), do: true
  def authorized?(action, %User{id: id}, %{recipe: recipe}) when action in [:update, :delete], do: recipe.owner_id == id

  def authorized?(_, _, _), do: false
end
