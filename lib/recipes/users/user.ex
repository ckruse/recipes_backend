defmodule Recipes.Users.User do
  use Ecto.Schema
  use Waffle.Ecto.Schema

  import Ecto.Changeset
  import Recipes.Utils

  @type t :: %__MODULE__{
          email: String.t(),
          encrypted_password: String.t(),
          active: boolean(),
          name: String.t() | nil,
          role: :root | :user
        }

  schema "users" do
    field :email, :string
    field :encrypted_password, :string
    field :active, :boolean, default: false
    field :name, :string
    field :role, Ecto.Enum, values: [:root, :user], default: :user

    field :avatar, Recipes.Uploaders.Avatar.Type

    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true

    has_many :recipes, Recipes.Formulas.Recipe, foreign_key: :owner_id

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:email, :password, :active, :name, :role])
    |> cast_attachments(attrs, [:avatar])
    |> validate_length(:password, min: 12)
    |> put_password_hash()
    |> validate_required([:email, :role])
    |> validate_inclusion(:role, [:root, :user])
  end

  defp put_password_hash(%Ecto.Changeset{valid?: true, changes: %{password: pass}} = changeset) when is_present(pass),
    do: change(changeset, Argon2.add_hash(pass, hash_key: :encrypted_password))

  defp put_password_hash(changeset), do: changeset

  def password_changeset(user, attrs) do
    user
    |> cast(attrs, [:password, :password_confirmation])
    |> validate_length(:password, min: 12)
    |> validate_confirmation(:password)
    |> put_password_hash()
  end
end
