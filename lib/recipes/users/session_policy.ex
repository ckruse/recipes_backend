defmodule Recipes.Users.SessionPolicy do
  @behaviour Recipes.Policies

  alias Recipes.Users.User

  @impl Recipes.Policies
  def authorized?(:sign_in, nil, %{user: user}), do: user.active
  def authorized?(:refresh, %User{} = user, _), do: user.active

  def authorized?(_, _, _), do: false
end
