defmodule Recipes.Users.UsersPolicy do
  @behaviour Recipes.Policies

  alias Recipes.Users.User

  @impl Recipes.Policies
  def authorized?(_, %User{role: :root}, _), do: true

  def authorized?(:get, current_user, %{user: user}), do: user.id == current_user.id
  def authorized?(action, %User{id: id}, %{user: user}) when action in [:update, :delete], do: user.id == id

  def authorized?(_, _, _), do: false
end
