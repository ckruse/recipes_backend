defmodule Recipes.Users do
  @moduledoc """
  The Users context.
  """

  import Ecto.Query, warn: false
  alias Recipes.Repo

  alias Recipes.Users.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  @spec list_users(keyword()) :: list(User.t())
  def list_users(opts \\ []) do
    from(user in User, order_by: [asc: user.id])
    |> maybe_apply_search(opts[:search])
    |> Repo.Utils.apply_limit(opts[:limit], opts[:offset])
    |> Repo.all()
  end

  @spec count_users(keyword()) :: non_neg_integer()
  def count_users(opts \\ []) do
    from(user in User, select: count())
    |> maybe_apply_search(opts[:search])
    |> Repo.one!()
  end

  defp maybe_apply_search(query, nil), do: query

  defp maybe_apply_search(query, search) do
    search
    |> String.split(~r/\s+/)
    |> Enum.reduce(query, fn term, query ->
      query
      |> where([user], ilike(user.name, ^"%#{term}%"))
      |> or_where([user], ilike(user.email, ^"%#{term}%"))
    end)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_user!(any, keyword()) :: User.t()
  def get_user!(id, opts \\ []) do
    User
    |> Repo.get!(id)
    |> Repo.maybe_preload(opts[:with])
  end

  @spec get_user_by(keyword) :: User.t() | nil
  def get_user_by(clauses) do
    User
    |> Repo.get_by(Keyword.delete(clauses, :with))
    |> Repo.maybe_preload(clauses[:with])
  end

  @spec get_user(any, keyword()) :: User.t() | nil
  def get_user(id, opts \\ []) do
    User
    |> Repo.get(id)
    |> Repo.maybe_preload(opts[:with])
  end

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_user(map()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def create_user(attrs) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_user(User.t(), map()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  def update_password(%User{} = user, attrs) do
    user
    |> User.password_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_user(User.t()) :: {:ok, User.t()} | {:error, Ecto.Changeset.t()}
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  #
  # authentication
  #

  alias Recipes.Guardian

  def verify_password(%User{encrypted_password: hash}, password) when is_binary(password) and is_binary(hash),
    do: Argon2.verify_pass(password, hash)

  def verify_password(_, _),
    do: Argon2.no_user_verify()

  @spec token_sign_in(User.t(), String.t()) :: {:error, :unauthorized} | {:ok, User.t(), String.t(), map}
  def token_sign_in(user, password) do
    with true <- verify_password(user, password),
         {:ok, token, claims} <- Guardian.encode_and_sign(user) do
      {:ok, user, token, claims}
    else
      _ -> {:error, :unauthorized}
    end
  end
end
