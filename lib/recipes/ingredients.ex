defmodule Recipes.Ingredients do
  @moduledoc """
  The Ingredients context.
  """

  import Ecto.Query, warn: false
  alias Recipes.Repo

  import Recipes.Utils

  alias Recipes.Ingredients.Ingredient

  def data(), do: Dataloader.Ecto.new(Recipes.Repo, query: &query/2)
  def query(queryable, _params), do: queryable

  @doc """
  Returns the list of ingredients.

  ## Examples

      iex> list_ingredients()
      [%Ingredient{}, ...]

  """
  @spec list_ingredients(keyword()) :: list(Ingredient.t())
  def list_ingredients(opts \\ []) do
    from(ingredient in Ingredient, order_by: [asc: :name, asc: :id])
    |> apply_search(opts[:search])
    |> Repo.Utils.apply_limit(opts[:limit], opts[:offset])
    |> Repo.all()
    |> Repo.maybe_preload(opts[:with])
  end

  @spec count_ingredients(keyword()) :: list(Ingredient.t())
  def count_ingredients(opts \\ []) do
    from(ingredient in Ingredient, select: count())
    |> apply_search(opts[:search])
    |> Repo.one!()
  end

  defp apply_search(q, term) when is_present(term) do
    term
    |> String.split(~r/\s+/)
    |> Enum.reduce(q, fn term, q -> from(ingredient in q, where: ilike(ingredient.name, ^"%#{term}%")) end)
  end

  defp apply_search(q, _), do: q

  @doc """
  Gets a single ingredient.

  Raises `Ecto.NoResultsError` if the Ingredient does not exist.

  ## Examples

      iex> get_ingredient!(123)
      %Ingredient{}

      iex> get_ingredient!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_ingredient!(any(), keyword()) :: Ingredient.t()
  def get_ingredient!(id, opts \\ []) do
    Ingredient
    |> Repo.get!(id)
    |> Repo.maybe_preload(opts[:with])
  end

  @spec get_ingredient(any(), keyword()) :: Ingredient.t() | nil
  def get_ingredient(id, opts \\ []) do
    Ingredient
    |> Repo.get(id)
    |> Repo.maybe_preload(opts[:with])
  end

  @doc """
  Creates a ingredient.

  ## Examples

      iex> create_ingredient(%{field: value})
      {:ok, %Ingredient{}}

      iex> create_ingredient(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_ingredient(map()) :: {:ok, Ingredient.t()} | {:error, Ecto.Changeset.t()}
  def create_ingredient(attrs \\ %{}) do
    %Ingredient{}
    |> Ingredient.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a ingredient.

  ## Examples

      iex> update_ingredient(ingredient, %{field: new_value})
      {:ok, %Ingredient{}}

      iex> update_ingredient(ingredient, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_ingredient(Ingredient.t(), map()) :: {:ok, Ingredient.t()} | {:error, Ecto.Changeset.t()}
  def update_ingredient(%Ingredient{} = ingredient, attrs) do
    ingredient
    |> Repo.preload([:units])
    |> Ingredient.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a ingredient.

  ## Examples

      iex> delete_ingredient(ingredient)
      {:ok, %Ingredient{}}

      iex> delete_ingredient(ingredient)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_ingredient(Ingredient.t()) :: {:ok, Ingredient.t()} | {:error, Ecto.Changeset.t()}
  def delete_ingredient(%Ingredient{} = ingredient) do
    Repo.delete(ingredient)
  end

  @spec calories(Ingredient.t()) :: float
  def calories(ingredient),
    do: ingredient.alc * 7.1 + ingredient.carbs * 4.1 + ingredient.fat * 9.3 + ingredient.proteins * 4.1
end
