defmodule Recipes.Guardian do
  use Guardian, otp_app: :recipes

  alias Recipes.Users
  alias Recipes.Users.User

  def subject_for_token(%User{} = user, _claims), do: {:ok, to_string(user.id)}
  def subject_for_token(_, _), do: {:error, :unknown_resource}

  def resource_from_claims(%{"sub" => id}) do
    resource = Users.get_user_by(id: id, active: true)

    if is_nil(resource),
      do: {:error, :not_found},
      else: {:ok, resource}
  end

  def resource_from_claims(_claims), do: {:error, :not_found}
end
