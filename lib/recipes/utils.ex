defmodule Recipes.Utils do
  defguard is_present(v) when not is_nil(v) and v != "" and v != 0 and v != [] and v != %{} and v != false
  defguard is_blank(v) when is_nil(v) or v == "" or v == 0 or v == [] or v == %{} or v == false

  @spec blank?(any()) :: boolean()
  def blank?(nil), do: true
  def blank?(""), do: true
  def blank?(0), do: true
  def blank?(false), do: true
  def blank?([]), do: true
  def blank?(%Ecto.Association.NotLoaded{}), do: true
  def blank?(map) when map == %{}, do: true
  def blank?(_), do: false

  @doc """
  Returns true for all non-blank values

  ### Examples

      iex> present?("foo")
      true

      iex> present?("")
      false
  """
  @spec present?(any()) :: boolean()
  def present?(v), do: not blank?(v)

  @doc """
  Converts values to integer, depending on the value itself:

  - When nil, empty string, empty list or empty map return 0
  - When true return 1
  - When value is a String try to parse it to an Integer
  - Return the value itself when it is an Integer
  - Return zero otherwise

  ### Examples

      iex> to_int(10)
      10

      iex> to_int("10")
      10

      iex> to_int(3.1)
      3
  """
  @spec to_int(any()) :: integer()
  def to_int(v) when is_nil(v), do: 0
  def to_int(""), do: 0
  def to_int([]), do: 0
  def to_int(true), do: 1
  def to_int(false), do: 0
  def to_int(%Decimal{} = v), do: Decimal.to_integer(v)
  def to_int(map) when map == %{}, do: 0
  def to_int(v) when is_bitstring(v), do: String.to_integer(v)
  def to_int(v) when is_integer(v), do: v
  def to_int(v) when is_number(v), do: trunc(v)
  def to_int(_v), do: 0

  @doc """
  Converts values to float, depending on the value itself:

  - When nil, empty string, empty list or empty map return 0.0
  - When true return 1.0
  - When value is a String try to parse it to an Float
  - Return the value itself when it is an Float
  - Return zero otherwise

  ### Examples

      iex> to_float(10)
      10.0

      iex> to_float("10")
      10.0

      iex> to_float(3.1)
      3.0
  """
  @spec to_float(any()) :: float()
  def to_float(v) when is_nil(v), do: 0.0
  def to_float(""), do: 0.0
  def to_float([]), do: 0.0
  def to_float(true), do: 1.0
  def to_float(false), do: 0.0
  def to_float(%Decimal{} = v), do: Decimal.to_float(v)
  def to_float(map) when map == %{}, do: 0.0
  def to_float(v) when is_bitstring(v), do: String.to_float(v)
  def to_float(v) when is_float(v), do: v
  def to_float(v) when is_number(v), do: v / 1
  def to_float(_v), do: 0.0

  @spec to_bool(any()) :: boolean()
  def to_bool(true), do: true
  def to_bool(num) when is_number(num) and num != 0, do: true
  def to_bool(map) when is_map(map) and map != %{}, do: true
  def to_bool(list) when is_list(list) and list != [], do: true
  def to_bool("false"), do: false
  def to_bool("0"), do: false
  def to_bool(str) when is_bitstring(str) and str != "", do: true
  def to_bool(_), do: false

  @spec is_env(:prod | :dev | :test) :: boolean()
  def is_env(env), do: Application.get_env(:acd, :environment) == env

  @spec prod?() :: boolean()
  def prod?(), do: is_env(:prod)

  @spec dev?() :: boolean()
  def dev?(), do: is_env(:dev)

  @spec async(fun()) :: any()
  def async(fun) do
    if is_env(:test),
      do: fun.(),
      else: Task.start(fun)
  end

  @spec get_value(map(), atom()) :: any()
  def get_value(map, atom_key), do: Map.get(map, atom_key) || Map.get(map, Atom.to_string(atom_key))

  def nil_or_string(val)
  def nil_or_string(nil), do: nil
  def nil_or_string(val), do: "#{val}"

  @spec ensure_path(Access.t(), [String.t() | number | atom], any) :: Access.t()
  def ensure_path(root, path, value \\ %{}) do
    if get_in(root, path) == nil,
      do: put_in(root, Enum.map(path, &Access.key(&1, %{})), value),
      else: root
  end
end
