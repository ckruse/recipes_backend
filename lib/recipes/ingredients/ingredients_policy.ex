defmodule Recipes.Ingredients.IngredientsPolicy do
  @behaviour Recipes.Policies

  alias Recipes.Users.User

  @impl Recipes.Policies
  def authorized?(:list, _, _), do: true
  def authorized?(:get, _, _), do: true

  def authorized?(_, %User{role: :root}, _), do: true

  def authorized?(_, _, _), do: false
end
