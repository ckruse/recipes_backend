defmodule Recipes.Ingredients.Unit do
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{
          id: non_neg_integer(),
          ingredient_id: non_neg_integer(),
          identifier: :pcs | :tbsp | :tsp | :skosh | :pinch,
          base_value: float(),
          inserted_at: NaiveDateTime.t(),
          updated_at: NaiveDateTime.t(),
          ingredient: Recipes.Ingredients.Ingredient.t()
        }

  schema "ingredient_units" do
    field :identifier, Ecto.Enum, values: [:pcs, :tbsp, :tsp, :skosh, :pinch]
    field :base_value, :float

    belongs_to :ingredient, Recipes.Ingredients.Ingredient

    timestamps()
  end

  @doc false
  def changeset(unit, attrs) do
    unit
    |> cast(attrs, [:ingredient_id, :identifier, :base_value])
    |> validate_required([:identifier, :base_value])
  end
end
