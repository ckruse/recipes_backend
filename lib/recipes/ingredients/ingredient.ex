defmodule Recipes.Ingredients.Ingredient do
  use Ecto.Schema
  import Ecto.Changeset

  @type t :: %__MODULE__{
          name: String.t(),
          reference: :g | :ml,
          alc: float(),
          carbs: float(),
          fat: float(),
          proteins: float(),
          step_ingredients: [Recipes.Steps.StepIngredient.t()],
          units: [Recipes.Ingredients.Unit.t()],
          inserted_at: NaiveDateTime.t(),
          updated_at: NaiveDateTime.t()
        }

  schema "ingredients" do
    field :name, :string
    field :reference, Ecto.Enum, values: [:ml, :g], default: :g
    field :alc, :float, default: 0.0
    field :carbs, :float, default: 0.0
    field :fat, :float, default: 0.0
    field :proteins, :float, default: 0.0

    has_many :step_ingredients, Recipes.Formulas.StepIngredient
    has_many :units, Recipes.Ingredients.Unit

    timestamps()
  end

  @doc false
  def changeset(ingredient, attrs) do
    ingredient
    |> cast(attrs, [:name, :reference, :carbs, :fat, :proteins, :alc])
    |> cast_assoc(:units, with: &Recipes.Ingredients.Unit.changeset/2)
    |> validate_required([:name, :reference, :carbs, :fat, :proteins, :alc])
  end
end
