defmodule Recipes.Formulas do
  @moduledoc """
  The Formulas context.
  """

  import Ecto.Query, warn: false
  alias Recipes.Repo

  import Recipes.Utils

  alias Recipes.Ingredients
  alias Recipes.Formulas.Recipe

  def data(), do: Dataloader.Ecto.new(Recipes.Repo, query: &query/2)
  def query(queryable, _params), do: queryable

  @doc """
  Returns the list of recipes.

  ## Examples

      iex> list_recipes()
      [%Recipe{}, ...]

  """
  @spec list_recipes(keyword()) :: list(Recipe.t())
  def list_recipes(opts \\ []) do
    from(recipe in Recipe, order_by: [asc: recipe.name, asc: recipe.id])
    |> apply_search(opts[:search])
    |> apply_tags_search(opts[:tags])
    |> Repo.Utils.apply_limit(opts[:limit], opts[:offset])
    |> Repo.all()
    |> Repo.maybe_preload(opts[:with])
  end

  @spec count_recipes(keyword()) :: non_neg_integer()
  def count_recipes(opts \\ []) do
    from(recipe in Recipe, select: count())
    |> apply_search(opts[:search])
    |> apply_tags_search(opts[:tags])
    |> Repo.one!()
  end

  defp apply_search(q, term) when is_present(term) do
    term
    |> String.split(~r/\s+/)
    |> Enum.reduce(q, fn term, q -> from(recipe in q, where: ilike(recipe.name, ^"%#{term}%")) end)
  end

  defp apply_search(q, _), do: q

  defp apply_tags_search(q, keywords) when is_present(keywords) do
    keywords
    |> Enum.reduce(q, fn term, q ->
      from(recipe in q,
        where:
          fragment(
            "? IN (SELECT recipe_id FROM recipes_tags INNER JOIN tags ON recipes_tags.tag_id = tags.id WHERE LOWER(tags.name) = LOWER(?))",
            recipe.id,
            ^term
          )
      )
    end)
  end

  defp apply_tags_search(q, _), do: q

  @doc """
  Gets a single recipe.

  Raises `Ecto.NoResultsError` if the Recipe does not exist.

  ## Examples

      iex> get_recipe!(123)
      %Recipe{}

      iex> get_recipe!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_recipe!(any(), keyword()) :: Recipe.t()
  def get_recipe!(id, opts \\ []) do
    Recipe
    |> Repo.get!(id)
    |> Repo.maybe_preload(opts[:with])
  end

  @spec get_recipe(any(), keyword()) :: Recipe.t() | nil
  def get_recipe(id, opts \\ []) do
    Recipe
    |> Repo.get(id)
    |> Repo.maybe_preload(opts[:with])
  end

  @doc """
  Creates a recipe.

  ## Examples

      iex> create_recipe(%{field: value})
      {:ok, %Recipe{}}

      iex> create_recipe(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_recipe(Recipes.Users.User.t(), map()) :: {:ok, Recipe.t()} | {:error, Ecto.Changeset.t()}
  def create_recipe(user, attrs \\ %{}) do
    %Recipe{}
    |> Recipe.changeset(attrs, user)
    |> Repo.insert()
  end

  @doc """
  Updates a recipe.

  ## Examples

      iex> update_recipe(recipe, %{field: new_value})
      {:ok, %Recipe{}}

      iex> update_recipe(recipe, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_recipe(Recipe.t(), map()) :: {:ok, Recipe.t()} | {:error, Ecto.Changeset.t()}
  def update_recipe(%Recipe{} = recipe, attrs) do
    recipe
    |> Repo.preload([:tags])
    |> Recipe.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a recipe.

  ## Examples

      iex> delete_recipe(recipe)
      {:ok, %Recipe{}}

      iex> delete_recipe(recipe)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_recipe(Recipe.t()) :: {:ok, Recipe.t()} | {:error, Ecto.Changeset.t()}
  def delete_recipe(%Recipe{} = recipe) do
    Repo.delete(recipe)
  end

  def calories(recipe) do
    recipe
    |> Repo.preload(steps: [step_ingredients: [:unit, :ingredient]])
    |> then(& &1.steps)
    |> Enum.flat_map(& &1.step_ingredients)
    |> Enum.map(fn
      %{unit_id: nil} = step_ingredient ->
        step_ingredient.amount * (Ingredients.calories(step_ingredient.ingredient) / 100.0)

      step_ingredient ->
        step_ingredient.unit.base_value * step_ingredient.amount *
          (Ingredients.calories(step_ingredient.ingredient) / 100.0)
    end)
    |> Enum.sum()
  end

  def batch_preload(_, recipes) do
    recipes
    |> Repo.preload(steps: [step_ingredients: [:unit, :ingredient]])
    |> Enum.reduce(%{}, fn recipe, acc ->
      Map.put(acc, recipe.id, recipe)
    end)
  end

  alias Recipes.Formulas.Step

  @doc """
  Returns the list of steps.

  ## Examples

      iex> list_steps()
      [%Step{}, ...]

  """
  @spec list_steps(Recipe.t(), keyword()) :: list(Step.t())
  def list_steps(recipe, opts \\ []) do
    from(step in Step, where: step.recipe_id == ^recipe.id, order_by: [asc: :position, asc: :id])
    |> Repo.all()
    |> Repo.maybe_preload(opts[:with])
  end

  @doc """
  Gets a single step.

  Raises `Ecto.NoResultsError` if the Step does not exist.

  ## Examples

      iex> get_step!(123)
      %Step{}

      iex> get_step!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_step!(any(), keyword()) :: Step.t()
  def get_step!(id, opts \\ []) do
    Step
    |> Repo.get!(id)
    |> Repo.maybe_preload(opts[:with])
  end

  @spec get_step(any(), keyword()) :: Step.t() | nil
  def get_step(recipe, id, opts \\ []) do
    Step
    |> Repo.get_by(recipe_id: recipe.id, id: id)
    |> Repo.maybe_preload(opts[:with])
  end

  @doc """
  Creates a step.

  ## Examples

      iex> create_step(%{field: value})
      {:ok, %Step{}}

      iex> create_step(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_step(Recipe.t(), map()) :: {:ok, Step.t()} | {:error, Ecto.Changeset.t()}
  def create_step(recipe, attrs \\ %{}) do
    %Step{}
    |> Step.changeset(attrs, recipe)
    |> Repo.insert()
  end

  @doc """
  Updates a step.

  ## Examples

      iex> update_step(step, %{field: new_value})
      {:ok, %Step{}}

      iex> update_step(step, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_step(Step.t(), map()) :: {:ok, Step.t()} | {:error, Ecto.Changeset.t()}
  def update_step(%Step{} = step, attrs) do
    step
    |> Repo.preload([:step_ingredients])
    |> Step.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a step.

  ## Examples

      iex> delete_step(step)
      {:ok, %Step{}}

      iex> delete_step(step)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_step(Step.t()) :: {:ok, Step.t()} | {:error, Ecto.Changeset.t()}
  def delete_step(%Step{} = step) do
    Repo.delete(step)
  end

  alias Recipes.Formulas.StepIngredient

  @doc """
  Returns the list of steps_ingridients.

  ## Examples

      iex> list_steps_ingridients()
      [%StepIngredient{}, ...]

  """
  @spec list_step_ingridients(Step.t(), keyword()) :: list(StepIngredient.t())
  def list_step_ingridients(step, opts \\ []) do
    from(step_ingredient in StepIngredient, where: step_ingredient.step_id == ^step.id, order_by: [asc: :id])
    |> Repo.all()
    |> Repo.maybe_preload(opts[:with])
  end

  @doc """
  Gets a single step_ingredients.

  Raises `Ecto.NoResultsError` if the Step ingredients does not exist.

  ## Examples

      iex> get_step_ingredients!(123)
      %StepIngredient{}

      iex> get_step_ingredients!(456)
      ** (Ecto.NoResultsError)

  """
  @spec get_step_ingredient!(any(), keyword()) :: StepIngredient.t()
  def get_step_ingredient!(id, opts \\ []) do
    StepIngredient
    |> Repo.get!(id)
    |> Repo.maybe_preload(opts[:with])
  end

  @spec get_step_ingredient(any(), keyword()) :: StepIngredient.t() | nil
  def get_step_ingredient(id, opts \\ []) do
    StepIngredient
    |> Repo.get(id)
    |> Repo.maybe_preload(opts[:with])
  end

  @doc """
  Creates a step_ingredients.

  ## Examples

      iex> create_step_ingredients(%{field: value})
      {:ok, %StepIngredient{}}

      iex> create_step_ingredients(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec create_step_ingredient(map()) :: {:ok, StepIngredient.t()} | {:error, Ecto.Changeset.t()}
  def create_step_ingredient(attrs \\ %{}) do
    %StepIngredient{}
    |> StepIngredient.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a step_ingredients.

  ## Examples

      iex> update_step_ingredients(step_ingredients, %{field: new_value})
      {:ok, %StepIngredient{}}

      iex> update_step_ingredients(step_ingredients, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  @spec update_step_ingredient(StepIngredient.t(), map()) :: {:ok, StepIngredient.t()} | {:error, Ecto.Changeset.t()}
  def update_step_ingredient(%StepIngredient{} = step_ingredients, attrs) do
    step_ingredients
    |> StepIngredient.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a step_ingredients.

  ## Examples

      iex> delete_step_ingredients(step_ingredients)
      {:ok, %StepIngredient{}}

      iex> delete_step_ingredients(step_ingredients)
      {:error, %Ecto.Changeset{}}

  """
  @spec delete_step_ingredient(StepIngredient.t()) :: {:ok, StepIngredient.t()} | {:error, Ecto.Changeset.t()}
  def delete_step_ingredient(%StepIngredient{} = step_ingredients) do
    Repo.delete(step_ingredients)
  end
end
