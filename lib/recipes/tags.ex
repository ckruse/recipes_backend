defmodule Recipes.Tags do
  @moduledoc """
  The Tags context.
  """

  import Ecto.Query, warn: false
  alias Recipes.Repo

  import Recipes.Utils

  alias Recipes.Tags.Tag

  def data(), do: Dataloader.Ecto.new(Recipes.Repo, query: &query/2)
  def query(queryable, _params), do: queryable

  @doc """
  Returns the list of tags.

  ## Examples

      iex> list_tags()
      [%Tag{}, ...]

  """
  @spec list_tags(keyword) :: [Tag.t()]
  def list_tags(opts \\ []) do
    from(tag in Tag, order_by: [asc: :name, asc: :id])
    |> Repo.Utils.apply_limit(opts[:limit], opts[:offset])
    |> maybe_search(opts[:search])
    |> Repo.all()
    |> Repo.maybe_preload(opts[:with])
  end

  defp maybe_search(q, term) when is_present(term) do
    term
    |> String.split(~r/\s+/)
    |> Enum.reduce(q, fn term, q -> where(q, [t], ilike(t.name, ^"%#{term}%")) end)
  end

  defp maybe_search(q, _), do: q

  @doc """
  Gets a single tag.

  Raises `Ecto.NoResultsError` if the Tag does not exist.

  ## Examples

      iex> get_tag!(123)
      %Tag{}

      iex> get_tag!(456)
      ** (Ecto.NoResultsError)

  """
  def get_tag!(id, opts \\ []) do
    Tag
    |> Repo.get!(id)
    |> Repo.maybe_preload(opts[:with])
  end

  def get_tag(id, opts \\ []) do
    Tag
    |> Repo.get(id)
    |> Repo.maybe_preload(opts[:with])
  end

  @spec get_tags(any) :: [Tag.t()]
  def get_tags(ids) do
    from(t in Tag, where: t.id in ^ids)
    |> Repo.all()
  end

  @doc """
  Creates a tag.

  ## Examples

      iex> create_tag(%{field: value})
      {:ok, %Tag{}}

      iex> create_tag(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_tag(attrs \\ %{}) do
    %Tag{}
    |> Tag.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a tag.

  ## Examples

      iex> update_tag(tag, %{field: new_value})
      {:ok, %Tag{}}

      iex> update_tag(tag, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_tag(%Tag{} = tag, attrs) do
    tag
    |> Tag.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a tag.

  ## Examples

      iex> delete_tag(tag)
      {:ok, %Tag{}}

      iex> delete_tag(tag)
      {:error, %Ecto.Changeset{}}

  """
  def delete_tag(%Tag{} = tag) do
    Repo.delete(tag)
  end
end
