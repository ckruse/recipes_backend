defmodule RecipesWeb.RecipeController do
  use RecipesWeb, :controller

  alias Recipes.Formulas
  alias Recipes.Formulas.Recipe
  alias Recipes.Ingredients.Unit

  def show(conn, %{"id" => id} = params) do
    portions =
      cond do
        params["portions"] == nil -> 1
        String.match?(params["portions"], ~r/^\d+$/) -> String.to_integer(params["portions"])
        true -> 1
      end

    valid_portions = if portions == 0, do: 1, else: portions

    id
    |> String.replace(~r/\.json$/, "")
    |> Formulas.get_recipe(with: [:owner, steps: [step_ingredients: [:ingredient, :unit]]])
    |> case do
      %Recipe{} = recipe ->
        recipe_map = %{
          "name" => recipe.name,
          "author" => recipe.owner.name || recipe.owner.email,
          "items" =>
            recipe.steps
            |> Enum.map(& &1.step_ingredients)
            |> List.flatten()
            |> Enum.map(&%{"itemId" => &1.ingredient.name, "spec" => calc_amount(&1, valid_portions)})
        }

        json = Jason.encode!(recipe_map)

        conn
        |> put_resp_content_type("application/json")
        |> send_resp(200, json)

      nil ->
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(
          404,
          %{"status" => "not found", message: "The requested recipe could not be found"} |> Jason.encode!()
        )
    end
  end

  defp calc_amount(%{amount: amount, unit: %Unit{identifier: ident, base_value: multiplier}}, portions),
    do: "#{amount * portions}×#{ident} (#{amount * portions * multiplier}g)"

  defp calc_amount(%{amount: amount}, portions), do: "#{amount * portions}g"
end
