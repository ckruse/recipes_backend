defmodule RecipesWeb.Router do
  use RecipesWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :jwt_authenticated do
    plug(:fetch_cookies)
    plug(RecipesWeb.Plug.AuthPipeline)
  end

  pipeline :graphql do
    plug(RecipesWeb.Plug.GraphQL)
  end

  scope "/" do
    pipe_through [:jwt_authenticated, :graphql, :api]

    forward "/graphql", Absinthe.Plug, schema: RecipesWeb.Schema
    get "/recipes/:id/bring.json", RecipesWeb.RecipeController, :show
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/" do
      forward "/graphiql", Absinthe.Plug.GraphiQL, schema: RecipesWeb.Schema
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
