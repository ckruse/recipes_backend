defmodule RecipesWeb.Middlewares.Authenticated do
  @behaviour Absinthe.Middleware

  alias Recipes.Users.User

  def call(resolution = %{context: %{current_user: %User{}}}, _), do: resolution
  def call(resolution, _), do: Absinthe.Resolution.put_result(resolution, {:error, :not_authenticated})
end
