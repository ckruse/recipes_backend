defmodule RecipesWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :recipes

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :recipes,
    gzip: false,
    only: ~w(assets fonts images favicon.ico robots.txt)

  plug RecipesWeb.Plug.Static

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    plug Phoenix.CodeReloader
    plug Phoenix.Ecto.CheckRepoStatus, otp_app: :recipes
  end

  plug Phoenix.LiveDashboard.RequestLogger,
    param_key: "request_logger",
    cookie_key: "request_logger"

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  if Mix.env() == :dev do
    plug(Corsica,
      origins: ~r{http://localhost(:\d+)?},
      allow_headers: :all,
      allow_credentials: true,
      log: [rejected: :error, invalid: :warn, accepted: :debug],
      max_age: 604_800
    )
  end

  plug RecipesWeb.Router
end
