defmodule RecipesWeb.Schema.Tags do
  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]
  import AbsintheErrorPayload.Payload

  alias RecipesWeb.Resolvers.TagsResolver

  object :tag do
    field :id, non_null(:id)
    field :name, non_null(:string)

    field :updated_at, non_null(:naive_datetime)
    field :inserted_at, non_null(:naive_datetime)

    field :recipes, list_of(:recipe), resolve: dataloader(Recipes.Formulas)
  end

  input_object :tag_input do
    field :name, :string
  end

  payload_object(:tag_payload, :tag)

  object :tag_mutations do
    field :mutate_tag, :tag_payload do
      arg(:id, :id)
      arg(:tag, non_null(:tag_input))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&TagsResolver.update_or_create/3)
      middleware(&build_payload/2)
    end

    field :delete_tag, :tag_payload do
      arg(:id, non_null(:id))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&TagsResolver.delete/3)
      middleware(&build_payload/2)
    end
  end

  object :tag_queries do
    field :tags, list_of(:tag) do
      arg(:search, :string)
      arg(:limit, non_null(:integer))
      arg(:offset, non_null(:integer))

      resolve(&TagsResolver.list/3)
    end

    field :tag, :tag do
      arg(:id, non_null(:id))

      resolve(&TagsResolver.get/3)
    end
  end
end
