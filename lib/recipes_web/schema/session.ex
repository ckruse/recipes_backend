defmodule RecipesWeb.Schema.Session do
  use Absinthe.Schema.Notation
  import AbsintheErrorPayload.Payload

  alias RecipesWeb.Resolvers.SessionResolver

  object :session do
    field(:token, :string)
    field(:user, :user)
  end

  payload_object(:session_payload, :session)

  object :session_mutations do
    field :login, :session_payload do
      arg(:email, non_null(:string))
      arg(:password, non_null(:string))

      resolve(&SessionResolver.login/3)

      middleware(fn resolution, _ ->
        with %{value: %{user: user}} <- resolution do
          update_in(resolution.context, &Map.put(&1, :current_user, user))
        end
      end)

      middleware(&build_payload/2)
    end

    field :refresh, :session_payload do
      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&SessionResolver.refresh/3)

      middleware(fn resolution, _ ->
        with %{value: %{user: user}} <- resolution do
          update_in(resolution.context, &Map.put(&1, :current_user, user))
        end
      end)

      middleware(&build_payload/2)
    end
  end
end
