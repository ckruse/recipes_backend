defmodule RecipesWeb.Schema.Formulas do
  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1, batch: 3]
  import AbsintheErrorPayload.Payload

  alias RecipesWeb.Resolvers.FormulasResolver

  object :recipe_image do
    field :thumb, :string
    field :medium, :string
    field :original, :string
  end

  object :recipe do
    field :id, non_null(:id)
    field :name, non_null(:string)
    field :description, :string

    field :image, :recipe_image do
      resolve(fn recipe, _, _ ->
        {:ok, Recipes.Uploaders.RecipeImage.urls({recipe.image, recipe})}
      end)
    end

    field :owner_id, non_null(:id)

    field :inserted_at, non_null(:naive_datetime)
    field :updated_at, non_null(:naive_datetime)

    field :steps, non_null(list_of(non_null(:step))), resolve: dataloader(Recipes.Formulas)
    field :tags, non_null(list_of(non_null(:tag))), resolve: dataloader(Recipes.Tags)
    field :owner, non_null(:user), resolve: dataloader(Recipes.Users)

    field :calories, non_null(:float) do
      resolve(fn recipe, _, _ ->
        batch({Recipes.Formulas, :batch_preload}, recipe, fn batch_results ->
          {:ok, Recipes.Formulas.calories(Map.get(batch_results, recipe.id))}
        end)
      end)
    end
  end

  object :step do
    field :id, non_null(:id)
    field :position, non_null(:integer)
    field :preparation_time, non_null(:integer)
    field :cooking_time, non_null(:integer)
    field :description, non_null(:string)

    field :inserted_at, non_null(:naive_datetime)
    field :updated_at, non_null(:naive_datetime)

    field :step_ingredients, non_null(list_of(non_null(:step_ingredient))), resolve: dataloader(Recipes.Formulas)
  end

  object :step_ingredient do
    field :id, non_null(:id)
    field :amount, non_null(:float)

    field :inserted_at, non_null(:naive_datetime)
    field :updated_at, non_null(:naive_datetime)

    field :unit_id, :id
    field :step_id, non_null(:id)
    field :ingredient_id, non_null(:id)

    field :unit, :unit, resolve: dataloader(Recipes.Ingredients)
    field :step, non_null(:step), resolve: dataloader(Recipes.Formulas)
    field :ingredient, non_null(:ingredient), resolve: dataloader(Recipes.Ingredients)
  end

  input_object :recipe_input do
    field :name, :string
    field :description, :string
    field :image, :upload

    field :tags, list_of(:id)
  end

  input_object :step_ingredient_input do
    field :id, :id
    field :amount, :float
    field :unit_id, :id
    field :ingredient_id, :id
  end

  input_object :step_input do
    field :position, :integer
    field :preparation_time, :integer
    field :cooking_time, :integer
    field :description, :string
    field :step_ingredients, list_of(:step_ingredient_input)
  end

  payload_object(:recipe_payload, :recipe)
  payload_object(:step_payload, :step)

  object :formula_mutations do
    field :mutate_recipe, :recipe_payload do
      arg(:id, :id)
      arg(:recipe, non_null(:recipe_input))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&FormulasResolver.update_or_create/3)
      middleware(&build_payload/2)
    end

    field :delete_recipe, :recipe_payload do
      arg(:id, non_null(:id))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&FormulasResolver.delete/3)
      middleware(&build_payload/2)
    end

    field :mutate_step, :step_payload do
      arg(:recipe_id, non_null(:id))
      arg(:id, :id)
      arg(:step, non_null(:step_input))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&FormulasResolver.update_or_create_step/3)
      middleware(&build_payload/2)
    end
  end

  object :formula_queries do
    field :recipes, list_of(:recipe) do
      arg(:search, :string)
      arg(:tags, list_of(:string))

      arg(:limit, non_null(:integer))
      arg(:offset, non_null(:integer))

      resolve(&FormulasResolver.list/3)
    end

    field :count_recipes, :integer do
      arg(:search, :string)
      arg(:tags, list_of(:string))

      resolve(&FormulasResolver.count/3)
    end

    field :recipe, :recipe do
      arg(:id, non_null(:id))

      resolve(&FormulasResolver.get/3)
    end
  end
end
