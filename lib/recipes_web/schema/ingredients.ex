defmodule RecipesWeb.Schema.Ingredients do
  use Absinthe.Schema.Notation
  import Absinthe.Resolution.Helpers, only: [dataloader: 1]
  import AbsintheErrorPayload.Payload

  alias RecipesWeb.Resolvers.IngredientsResolver

  enum :reference_enum do
    value(:g)
    value(:ml)
  end

  enum :unit_identifier_enum do
    value(:pcs)
    value(:tbsp)
    value(:tsp)
    value(:skosh)
    value(:pinch)
  end

  object :unit do
    field :id, non_null(:id)
    field :identifier, non_null(:unit_identifier_enum)
    field :base_value, non_null(:float)

    field :inserted_at, non_null(:naive_datetime)
    field :updated_at, non_null(:naive_datetime)

    field :ingredient_id, non_null(:id)
    field :ingredient, non_null(:ingredient), resolve: dataloader(Recipes.Ingredients)
  end

  object :ingredient do
    field :id, non_null(:id)
    field :name, non_null(:string)
    field :reference, non_null(:reference_enum)
    field :alc, non_null(:float)
    field :carbs, non_null(:float)
    field :fat, non_null(:float)
    field :proteins, non_null(:float)

    field :calories, non_null(:float), resolve: &IngredientsResolver.base_calories/3

    field :inserted_at, non_null(:naive_datetime)
    field :updated_at, non_null(:naive_datetime)

    field :step_ingredients, non_null(list_of(non_null(:step_ingredient))), resolve: dataloader(Recipes.Formulas)
    field :units, non_null(list_of(non_null(:unit))), resolve: dataloader(Recipes.Ingredients)
  end

  input_object :unit_input do
    field :id, :id
    field :identifier, :unit_identifier_enum
    field :base_value, :float
  end

  input_object :ingredient_input do
    field :name, :string
    field :reference, :reference_enum
    field :alc, :float
    field :carbs, :float
    field :fat, :float
    field :proteins, :float

    field :units, list_of(:unit_input)
  end

  payload_object(:ingredient_payload, :ingredient)

  object :ingredient_mutations do
    field :delete_ingredient, :ingredient_payload do
      arg(:id, non_null(:id))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&IngredientsResolver.delete/3)
      middleware(&build_payload/2)
    end

    field :mutate_ingredient, :ingredient_payload do
      arg(:id, :id)
      arg(:ingredient, non_null(:ingredient_input))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&IngredientsResolver.update_or_create/3)
      middleware(&build_payload/2)
    end
  end

  object :ingredient_queries do
    field :ingredients, list_of(:ingredient) do
      arg(:search, :string)

      arg(:limit, non_null(:integer))
      arg(:offset, non_null(:integer))

      resolve(&IngredientsResolver.list/3)
    end

    field :count_ingredients, list_of(:ingredient) do
      arg(:search, :string)

      resolve(&IngredientsResolver.count/3)
    end

    field :ingredient, :ingredient do
      arg(:id, non_null(:id))
      resolve(&IngredientsResolver.get/3)
    end
  end
end
