defmodule RecipesWeb.Schema.Users do
  use Absinthe.Schema.Notation
  import AbsintheErrorPayload.Payload

  alias RecipesWeb.Resolvers.UserResolver

  object :user_avatar do
    field :thumb, :string
    field :original, :string
  end

  enum :user_role do
    value(:root)
    value(:user)
  end

  object :user do
    field :id, non_null(:id)
    field :email, non_null(:string)
    field :active, non_null(:boolean)
    field :role, non_null(:user_role)

    field :name, :string

    field(:avatar, :user_avatar) do
      resolve(fn user, _, _ ->
        {:ok, Recipes.Uploaders.Avatar.urls({user.avatar, user})}
      end)
    end

    field :inserted_at, non_null(:naive_datetime)
    field :updated_at, non_null(:naive_datetime)
  end

  input_object :user_input do
    field :email, :string
    field :active, :boolean
    field :name, :string
    field :password, :string
    field :avatar, :upload
    field :role, :user_role
  end

  payload_object(:user_payload, :user)

  object :user_mutations do
    field :mutate_user, :user_payload do
      arg(:id, :id)
      arg(:user, non_null(:user_input))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&UserResolver.update_or_create/3)
      middleware(&build_payload/2)
    end

    field :delete_user, :user_payload do
      arg(:id, non_null(:id))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&UserResolver.delete/3)
      middleware(&build_payload/2)
    end

    field :change_password, :user_payload do
      arg(:id, non_null(:id))
      arg(:current_password, non_null(:string))
      arg(:password, non_null(:string))
      arg(:password_confirmation, non_null(:string))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&UserResolver.update_password/3)
      middleware(&build_payload/2)
    end
  end

  object :user_queries do
    field :users, list_of(:user) do
      arg(:search, :string)

      arg(:limit, :integer, default_value: 50)
      arg(:offset, :integer, default_value: 0)

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&UserResolver.list/3)
    end

    field :count_users, :integer do
      arg(:search, :string)

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&UserResolver.count/3)
    end

    field :user, :user do
      arg(:id, non_null(:id))

      middleware(RecipesWeb.Middlewares.Authenticated)
      resolve(&UserResolver.get/3)
    end
  end
end
