defmodule RecipesWeb.Resolvers.UserResolver do
  import Recipes.Utils

  alias Recipes.Users
  alias Recipes.Users.User
  alias Recipes.Users.UsersPolicy

  alias Recipes.Policies

  def list(_, args, %{context: context}) do
    with :ok <- Policies.authorize(UsersPolicy, :list, context[:current_user], %{}) do
      {:ok, Users.list_users(args)}
    end
  end

  def count(_, args, %{context: context}) do
    with :ok <- Policies.authorize(UsersPolicy, :list, context[:current_user], %{}) do
      {:ok, Users.count_users(args)}
    end
  end

  def get(_, %{id: id}, %{context: context}) do
    with %User{} = user <- Users.get_user(id),
         :ok <- Policies.authorize(UsersPolicy, :get, context[:current_user], %{user: user}) do
      {:ok, user}
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def update_or_create(_, %{id: id, user: values}, %{context: context}) when is_present(id) do
    with %User{} = user <- Users.get_user(id),
         :ok <- Policies.authorize(UsersPolicy, :update, context[:current_user], %{user: user}) do
      Users.update_user(user, values)
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def update_or_create(_, %{user: values}, %{context: context}) do
    with :ok <- Policies.authorize(UsersPolicy, :create, context[:current_user], %{}) do
      Users.create_user(values)
    end
  end

  def update_password(
        _,
        %{current_password: current_password, password: password, password_confirmation: password_confirmation},
        %{context: context}
      ) do
    with :ok <- Policies.authorize(UsersPolicy, :update_password, context[:current_user], %{}),
         true <- Users.verify_password(context[:current_user], current_password) do
      Users.update_password(context[:current_user], %{password: password, password_confirmation: password_confirmation})
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def delete(_, %{id: id}, %{context: context}) do
    with %User{} = user <- Users.get_user(id),
         :ok <- Policies.authorize(UsersPolicy, :delete, context[:current_user], %{user: user}) do
      Users.delete_user(user)
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end
end
