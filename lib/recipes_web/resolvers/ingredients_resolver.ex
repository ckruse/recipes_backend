defmodule RecipesWeb.Resolvers.IngredientsResolver do
  import Recipes.Utils

  alias Recipes.Ingredients
  alias Recipes.Ingredients.Ingredient
  alias Recipes.Ingredients.IngredientsPolicy

  alias Recipes.Policies

  def list(_, args, %{context: context}) do
    with :ok <- Policies.authorize(IngredientsPolicy, :list, context[:current_user], %{}) do
      {:ok, Ingredients.list_ingredients(args)}
    end
  end

  def count(_, args, %{context: context}) do
    with :ok <- Policies.authorize(IngredientsPolicy, :list, context[:current_user], %{}) do
      {:ok, Ingredients.count_ingredients(args)}
    end
  end

  def get(_, %{id: id}, %{context: context}) do
    with %Ingredient{} = ingredient <- Ingredients.get_ingredient(id),
         :ok <- Policies.authorize(IngredientsPolicy, :get, context[:current_user], %{ingredient: ingredient}) do
      {:ok, ingredient}
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def update_or_create(_, %{id: id, ingredient: values}, %{context: context}) when is_present(id) do
    with %Ingredient{} = ingredient <- Ingredients.get_ingredient(id),
         :ok <- Policies.authorize(IngredientsPolicy, :update, context[:current_user], %{ingredient: ingredient}) do
      Ingredients.update_ingredient(ingredient, values)
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def update_or_create(_, %{ingredient: values}, %{context: context}) do
    with :ok <- Policies.authorize(IngredientsPolicy, :create, context[:current_user], %{}) do
      Ingredients.create_ingredient(values)
    end
  end

  def delete(_, %{id: id}, %{context: context}) do
    with %Ingredient{} = ingredient <- Ingredients.get_ingredient(id),
         :ok <- Policies.authorize(IngredientsPolicy, :delete, context[:current_user], %{ingredient: ingredient}) do
      Ingredients.delete_ingredient(ingredient)
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def base_calories(ingredient, _, _) do
    {:ok, Ingredients.calories(ingredient)}
  end
end
