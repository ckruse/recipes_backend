defmodule RecipesWeb.Resolvers.TagsResolver do
  import Recipes.Utils

  alias Recipes.Tags
  alias Recipes.Tags.Tag
  alias Recipes.Tags.TagsPolicy

  alias Recipes.Policies

  def list(_, args, %{context: context}) do
    with :ok <- Policies.authorize(TagsPolicy, :list, context[:current_user], %{}) do
      {:ok, Tags.list_tags(args)}
    end
  end

  def get(_, %{id: id}, %{context: context}) do
    with %Tag{} = tag <- Tags.get_tag(id),
         :ok <- Policies.authorize(TagsPolicy, :get, context[:current_user], %{tag: tag}) do
      {:ok, tag}
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def update_or_create(_, %{id: id, tag: values}, %{context: context}) when is_present(id) do
    with %Tag{} = tag <- Tags.get_tag(id),
         :ok <- Policies.authorize(TagsPolicy, :update, context[:current_user], %{tag: tag}) do
      Tags.update_tag(tag, values)
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def update_or_create(_, %{tag: values}, %{context: context}) do
    with :ok <- Policies.authorize(TagsPolicy, :create, context[:current_user], %{}) do
      Tags.create_tag(values)
    end
  end

  def delete(_, %{id: id}, %{context: context}) do
    with %Tag{} = tag <- Tags.get_tag(id),
         :ok <- Policies.authorize(TagsPolicy, :delete, context[:current_user], %{tag: tag}) do
      Tags.delete_tag(tag)
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end
end
