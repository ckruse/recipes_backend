defmodule RecipesWeb.Resolvers.SessionResolver do
  alias Recipes.Users
  alias Recipes.Users.User
  alias Recipes.Users.SessionPolicy

  alias Recipes.Policies

  def login(_, %{email: email, password: password}, %{context: context}) do
    with %User{} = user <- Users.get_user_by(email: email),
         :ok <- Policies.authorize(SessionPolicy, :sign_in, context[:current_user], %{user: user}),
         {:ok, user, token, _claims} <- Users.token_sign_in(user, password) do
      {:ok, %{user: user, token: token}}
    else
      _ -> {:error, :unauthorized}
    end
  end

  def refresh(_, _args, %{context: %{current_user: user, token: token}}) do
    with :ok <- Policies.authorize(SessionPolicy, :refresh, user, %{}),
         {:ok, _, {new_jwt, _new_claims}} <- Recipes.Guardian.refresh(token, ttl: {30, :days}),
         {:ok, user, _} <- Recipes.Guardian.resource_from_token(new_jwt) do
      {:ok, %{token: new_jwt, user: user}}
    else
      _ -> {:error, "unauthorized"}
    end
  end
end
