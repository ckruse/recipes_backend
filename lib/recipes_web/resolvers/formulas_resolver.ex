defmodule RecipesWeb.Resolvers.FormulasResolver do
  import Recipes.Utils

  alias Recipes.Formulas
  alias Recipes.Formulas.Recipe
  alias Recipes.Formulas.RecipesPolicy

  alias Recipes.Formulas.Step
  alias Recipes.Formulas.StepsPolicy

  alias Recipes.Policies

  def list(_, args, %{context: context}) do
    with :ok <- Policies.authorize(RecipesPolicy, :list, context[:current_user], %{}) do
      {:ok, Formulas.list_recipes(args)}
    end
  end

  def count(_, args, %{context: context}) do
    with :ok <- Policies.authorize(RecipesPolicy, :list, context[:current_user], %{}) do
      {:ok, Formulas.count_recipes(args)}
    end
  end

  def get(_, %{id: id}, %{context: context}) do
    with %Recipe{} = recipe <- Formulas.get_recipe(id),
         :ok <- Policies.authorize(RecipesPolicy, :get, context[:current_user], %{recipe: recipe}) do
      {:ok, recipe}
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def update_or_create(_, %{id: id, recipe: values}, %{context: context}) when is_present(id) do
    with %Recipe{} = recipe <- Formulas.get_recipe(id),
         :ok <- Policies.authorize(RecipesPolicy, :update, context[:current_user], %{recipe: recipe}) do
      Formulas.update_recipe(recipe, values)
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def update_or_create(_, %{recipe: values}, %{context: context}) do
    with :ok <- Policies.authorize(RecipesPolicy, :create, context[:current_user], %{}) do
      Formulas.create_recipe(context[:current_user], values)
    end
  end

  def delete(_, %{id: id}, %{context: context}) do
    with %Recipe{} = recipe <- Formulas.get_recipe(id),
         :ok <- Policies.authorize(RecipesPolicy, :delete, context[:current_user], %{recipe: recipe}) do
      Formulas.delete_recipe(recipe)
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def update_or_create_step(_, %{recipe_id: recipe_id, id: id, step: values}, %{context: context})
      when is_present(id) do
    with %Recipe{} = recipe <- Formulas.get_recipe(recipe_id),
         %Step{} = step <- Formulas.get_step(recipe, id),
         :ok <- Policies.authorize(StepsPolicy, :update, context[:current_user], %{recipe: recipe, step: step}) do
      Formulas.update_step(step, values)
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end

  def update_or_create_step(_, %{recipe_id: recipe_id, step: values}, %{context: context}) do
    with %Recipe{} = recipe <- Formulas.get_recipe(recipe_id),
         :ok <- Policies.authorize(StepsPolicy, :create, context[:current_user], %{}) do
      Formulas.create_step(recipe, values)
    else
      nil -> {:error, :not_found}
      v -> v
    end
  end
end
