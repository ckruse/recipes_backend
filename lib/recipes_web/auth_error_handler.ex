defmodule RecipesWeb.AuthErrorHandler do
  @moduledoc """
  returns an error when called
  """

  import Plug.Conn

  def auth_error(conn, {type, _reason}, opts) do
    if opts[:halt] == false do
      conn
    else
      conn
      |> put_status(401)
      |> Phoenix.Controller.json(%{error: to_string(type)})
    end
  end
end
