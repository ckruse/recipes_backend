defmodule Recipes.Uploaders.RecipeImage do
  use Waffle.Definition
  use Waffle.Ecto.Definition

  @versions [:original, :medium, :thumb]

  def validate({file, _}) do
    file_extension = file.file_name |> Path.extname() |> String.downcase()

    if Enum.member?(~w(.jpg .jpeg .gif .png), file_extension),
      do: :ok,
      else: {:error, "invalid file type"}
  end

  def transform(:thumb, _), do: {:convert, "-strip -thumbnail 250x250^ -gravity center -extent 250x250"}
  def transform(:medium, _), do: {:convert, "-strip -thumbnail 800x600^ -gravity center -extent 800x600"}

  def storage_dir_prefix, do: Application.get_env(:recipes, :recipe_image_dir)
  def storage_dir(_version, {_, scope}), do: "#{Application.get_env(:recipes, :recipe_image_url)}/#{scope.id}/"
  def default_url(_version, _), do: nil
end
