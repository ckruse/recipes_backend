defmodule Recipes.Uploaders.Avatar do
  use Waffle.Definition
  use Waffle.Ecto.Definition

  @versions [:original, :thumb]

  # Whitelist file extensions:
  def validate({file, _}) do
    file_extension = file.file_name |> Path.extname() |> String.downcase()

    if Enum.member?(~w(.jpg .jpeg .gif .png), file_extension),
      do: :ok,
      else: {:error, "invalid file type"}
  end

  # Define a thumbnail transformation:
  def transform(:thumb, _), do: {:convert, "-strip -thumbnail 250x250^ -gravity center -extent 250x250"}

  # Override the persisted filenames:
  # def filename(version, _) do
  #   version
  # end

  # Override the storage directory:
  # def storage_dir(version, {file, scope}) do
  #   "priv/uploads/user/avatars/#{scope.id}"
  # end

  def storage_dir_prefix, do: Application.get_env(:recipes, :avatar_dir)

  # Override the storage directory:
  def storage_dir(_version, {_, scope}), do: "#{Application.get_env(:recipes, :avatar_url)}/#{scope.id}/"

  # Provide a default URL if there hasn't been a file uploaded
  def default_url(version, _), do: "#{Application.get_env(:waffle, :asset_host)}/uploads/default_#{version}.png"

  # Specify custom headers for s3 objects
  # Available options are [:cache_control, :content_disposition,
  #    :content_encoding, :content_length, :content_type,
  #    :expect, :expires, :storage_class, :website_redirect_location]
  #
  # def s3_object_headers(version, {file, scope}) do
  #   [content_type: MIME.from_path(file.file_name)]
  # end
end
