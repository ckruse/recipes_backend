defmodule RecipesWeb.Plug.GraphQL do
  alias Recipes.Users.User

  def init(opts), do: opts

  def call(conn, _opts) do
    with %User{} = user <- Guardian.Plug.current_resource(conn),
         "Bearer " <> token when is_bitstring(token) <- fetch_token_from_header(conn) do
      Absinthe.Plug.put_options(conn, context: %{current_user: user, token: token})
    else
      _ -> conn
    end
  end

  defp fetch_token_from_header(conn) do
    headers = Plug.Conn.get_req_header(conn, "authorization")

    fetch_token_from_header(conn, headers)
  end

  defp fetch_token_from_header(_, []), do: nil
  defp fetch_token_from_header(_conn, [token | _tail]), do: String.trim(token)
end
