defmodule RecipesWeb.Plug.AuthPipeline do
  use Guardian.Plug.Pipeline,
    otp_app: :recipes,
    module: Recipes.Guardian,
    error_handler: RecipesWeb.AuthErrorHandler

  plug(Guardian.Plug.VerifySession)
  plug(Guardian.Plug.VerifyHeader, scheme: "Bearer", halt: false)
  # plug(Guardian.Plug.EnsureAuthenticated)
  plug(Guardian.Plug.LoadResource, allow_blank: true)
end
