defmodule RecipesWeb.Plug.Static do
  def init(opts), do: opts

  def call(conn, _opts) do
    opts =
      Plug.Static.init(
        at: "/uploads",
        from: Application.get_env(:recipes, :uploads_dir, Path.expand("./priv/uploads"))
      )

    Plug.Static.call(conn, opts)
  end
end
