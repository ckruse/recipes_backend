defmodule RecipesWeb.Schema do
  use Absinthe.Schema

  import_types(Absinthe.Plug.Types)
  import_types(Absinthe.Type.Custom)
  import_types(AbsintheErrorPayload.ValidationMessageTypes)

  import_types(RecipesWeb.Schema.Session)
  import_types(RecipesWeb.Schema.Users)
  import_types(RecipesWeb.Schema.Formulas)
  import_types(RecipesWeb.Schema.Ingredients)
  import_types(RecipesWeb.Schema.Tags)

  def context(ctx) do
    loader =
      Dataloader.new()
      |> Dataloader.add_source(Recipes.Formulas, Recipes.Formulas.data())
      |> Dataloader.add_source(Recipes.Ingredients, Recipes.Ingredients.data())
      |> Dataloader.add_source(Recipes.Tags, Recipes.Tags.data())

    Map.put(ctx, :loader, loader)
  end

  def plugins do
    [Absinthe.Middleware.Dataloader | Absinthe.Plugin.defaults()]
  end

  query do
    import_fields(:user_queries)
    import_fields(:formula_queries)
    import_fields(:tag_queries)
    import_fields(:ingredient_queries)
  end

  mutation do
    import_fields(:session_mutations)

    import_fields(:user_mutations)
    import_fields(:formula_mutations)
    import_fields(:tag_mutations)
    import_fields(:ingredient_mutations)
  end
end
